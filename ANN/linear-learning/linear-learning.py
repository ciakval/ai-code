#!/usr/bin/env python3

# File:     ANN/linear-learning.py
# Author:   Honza Remes (jan.remes@hig.no)
# Project:  AI Code

# DESCRIPTION #
#
# Demonstrates learning of a simple ANN consisting only of a single perceptron
# with identity activation function, bias and one input (a.k.a. 'linear
# perceptron')
#
# Uses 2D points in the DATA list and the "delta rule" to achieve "best fit"
# linear function.
#
# It is written in Python 3 (www.python.org)



DATA = [
        [ 0.3, 1.6 ],
        [ 0.35, 1.4 ],
        [ 0.4, 1.4 ],
        [ 0.5, 1.6 ],
        [ 0.6, 1.7 ],
        [ 0.8, 2.0 ],
        [ 0.95, 1.7 ],
        [ 1.1, 2.1 ]
        ]

M = 0.5
C = 0.5

RATE = 0.3

ITER = 0

ERROR = 99999

MIN = 0.05

def perceptron(x):
    return M * x  + C

running = True

print("Learning phase ...")

while running:
    for [ x, y ] in DATA:

        ITER += 1
        result = perceptron(x)
        ERROR = - (result - y)

        print("Iteration " + str(ITER) + ": ERROR is " + str(ERROR) + ". M = " + str(M) + ", C = " + str(C))

        if abs(ERROR) < MIN:
            running = False
            break

        delta_0 = RATE * ERROR * 1
        delta_1 = RATE * ERROR * x

        M = M + delta_1
        C = C + delta_0

print("Testing phase ...")


for [x,y] in DATA:
    result = perceptron(x)

    delta = abs(result - y)

    print("Data: [" + str(x) + "," + str(y) + "], result " + str(result) + ", error " + str(delta))


